from HTTPClient import NVPair
from env import request, extractAll, extract, valueOrDefault
from java.util.regex import Pattern

class ProjectAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : browse projects'),
            'add' : request(testId + 1, 'HTTP-REQ : add project'),
            'view' : request(testId + 2, 'HTTP-REQ : view project'),
            'edit' : request(testId + 3, 'HTTP-REQ : edit project')
        }
        self.patterns = {
            'project_avatars' : Pattern.compile('(?s)class="project-avatar.*?src="(.*?)"'),
            'add_project_user_avatar' : Pattern.compile('(\/secure\/useravatar.*?)\''),
            'view_project_avatar' : Pattern.compile('(?s)id="project-config-header-avatar".*?src="(.*?)"'),
            'edit_project_avatar' : Pattern.compile('(?s)id="project_avatar_id".*?src="(.*?)"'),
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_avatarId' : Pattern.compile('(?s)id="project_avatar_id".*?value="([0-9]*)"'),
            'edit_lead' : Pattern.compile('(?s)name="lead".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?\>(.*?)\<'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"'),
            'atl_token_dialog' : Pattern.compile('(?s)name="atl_token".*?value="(.*?)"')
        }
        
        
    def browse(self, cached=False):
        req = self.requests['browse']

        page = req.GET('/secure/project/ViewProjects.jspa').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            
            for avatar in extractAll(page, self.patterns['project_avatars']):
                req.GET(avatar)
        
        return extract(page, self.patterns['atl_token'])

        
    def add(self, project={}, cached=False):
        req = self.requests['add']
        
        dialog = req.GET('/secure/admin/AddProject!default.jspa?inline=true&decorator=dialog&_=1329796611415').text
        
        # can't use that on 4.4 for the whitepaper because of dataset permission error
#        if not cached:
#            req.GET(extract(dialog, self.patterns['add_project_user_avatar'], (('&amp;', '&'),)))

        req.POST('/secure/admin/AddProject.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('name', project['name']),
            NVPair('key', project['key']),
            NVPair('lead', project['lead']),
            NVPair('atl_token', extract(dialog, self.patterns['atl_token_dialog'])),
            NVPair('permissionScheme', '0'),
            NVPair('assigneeType', '2'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        return self.view(project['key'], cached)
        
        
    def view(self, projectKey, cached=False):
        req = self.requests['view']
        
        page = req.GET('/plugins/servlet/project-config/' + projectKey + '/summary').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/com.atlassian.jira.jira-project-config-plugin:project-config-global.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-summary/com.atlassian.jira.jira-project-config-plugin:project-config-summary.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/com.atlassian.jira.jira-project-config-plugin:project-config-global.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-summary/com.atlassian.jira.jira-project-config-plugin:project-config-summary.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon16-sprite.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-issuetypes.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/genericissue.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-workflows.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-screens.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-fields.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-settings.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-versions.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-permissions.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-people.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-components.png')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-notifications.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

            req.GET(extract(page, self.patterns['view_project_avatar']))

        return extract(page, self.patterns['atl_token'])
    

    def edit(self, token, project, cached=False):
        req = self.requests['edit']
        pid = project['id']
        
        dialog = req.GET('/secure/project/EditProject!default.jspa?pid=' + pid + '&inline=true&decorator=dialog&_=1329798180137').text

        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-icon-forms.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/wait.gif')
            req.GET(extract(dialog, self.patterns['edit_project_avatar']))

        req.POST('/secure/project/EditProject.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('name', valueOrDefault(project, 'name', extract(dialog, self.patterns['edit_name']))),
            NVPair('url', ''),
            NVPair('description', valueOrDefault(project, 'description', extract(dialog, self.patterns['edit_description']))),
            NVPair('atl_token', token),
            NVPair('avatarId', valueOrDefault(project, 'avatarId', extract(dialog, self.patterns['edit_avatarId']))),
            #this is not a mistake, JIRA really does put this parameter in twice.
            NVPair('avatarId', valueOrDefault(project, 'avatarId', extract(dialog, self.patterns['edit_avatarId']))),
            NVPair('pid', pid),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))

        return self.view(project['key'], cached)
        