from net.grinder.script.Grinder import grinder
from java.lang import System

class Config:
    
    def getIssueFile(self):
        return self.getFile('grinder.jira.config.issues', 'jira_issues.txt')
    
    def getProjectFile(self):
        return self.getFile('grinder.jira.config.projects', 'jira_projects.txt')

    def getUserFile(self):
        return self.getFile('grinder.jira.config.users', 'jira_users.txt')
    
    def shouldRecordHttp(self):
        return bool(self.getValue('grinder.jira.http.record'))
    
    def getTotalWorkers(self):
        return int(self.getValue('grinder.jira.workers.total'))
    
    def getCurrentWorker(self):
        return int(self.getValue('grinder.jira.workers.current'))
    
    def getValue(self, key):
        val = System.getProperty(key, 'false')
        
        if val == 'false':
            if not key in grinder.properties:
                raise 'key ' + key + ' missing, please specify either in grinder.properties or as a JVM parameter'
            
            val = grinder.properties[key]
        return val
            
    def getFile(self, key, default):
        val = System.getProperty(key, 'false')
        
        if val == 'false' and key in grinder.properties:
            val = grinder.properties[key]
        
        if val == 'false':
            val = default
            
        return val
        
            
        