from env import request, extract
from java.util.regex import Pattern

class Reports:
    
    def __init__(self, testId):
        self.requests = {
            'configure' : request(testId, 'HTTP REQ : configure report'),
            'report' : request(testId + 1, 'HTTP REQ : view report')
        }
        self.patterns = {
            'chart' : Pattern.compile('(\/charts\?filename=jfreechart-onetime-.*?\.png)')
        }
        
    def viewCreatedVsResolved(self, projectId, cached=False):
        req = self.requests['configure']
        
        req.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/1/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/1/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/1/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/1/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/1/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/1/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/s/en_US-64k3hp/664/1/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/1/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/images/border/spacer.gif')
            req.GET('/images/jira111x30.png')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/1/1/_/download/superbatch/css/images/toolbar/aui-toolbar-button-active-bg.png')
                
        req = self.requests['report']
        page = req.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&cumulative=true&versionLabels=major&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report&Next=Next').text
        req.GET(extract(page, self.patterns['chart']))
        
        
    def viewRecentlyCreatedIssues(self, projectId, cached=False):
        req = self.requests['configure']
        
        req.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/1/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/1/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/1/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/1/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/1/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/1/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/1/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/1/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_US-64k3hp/664/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/images/border/spacer.gif')
            req.GET('/images/jira111x30.png')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/1/1/_/download/superbatch/css/images/toolbar/aui-toolbar-button-active-bg.png')
        
        req = self.requests['report']        
        page = req.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report&Next=Next').text
        req.GET(extract(page, self.patterns['chart']))
        