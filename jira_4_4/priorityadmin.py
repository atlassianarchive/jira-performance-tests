from HTTPClient import NVPair
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class PriorityAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view priorities'),
            'add' : request(testId + 1, 'HTTP-REQ : add priority'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit priority')
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_color' : Pattern.compile('(?s)name="statusColor".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
            'edit_iconurl' : Pattern.compile('(?s)name="iconurl".*?value="(.*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewPriorities.jspa').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/images/border/spacer.gif')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/arrow_up_blue_small.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/bullet_creme.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/required.png')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

        return extract(page, self.patterns['atl_token'])
    

    def add(self, token, priority, cached=False):
        req = self.requests['add']
        
        req.GET('/secure/popups/IconPicker.jspa?fieldType=priority&formName=jiraform')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/jira111x30.png')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

        req.GET('/secure/popups/colorpicker.jsp?atl_token=' + token + '&element=statusColor')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:lookandfeel/jira.webresources:lookandfeel.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/toolbar/aui-toolbar-button-active-bg.png')
            
        req.POST('/secure/admin/AddPriority.jspa',
            (
                NVPair('atl_token', token),
                NVPair('name', priority['name']),
                NVPair('description', valueOrEmpty(priority, 'description')),
                NVPair('iconurl', valueOrDefault(priority, 'iconurl', '/images/icons/priority_major.gif')),
                NVPair('statusColor', valueOrDefault(priority, 'statusColor', '#9966cc')),
                NVPair('preview', 'false'),
                NVPair('Add', 'Add'),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
        
        self.browse(cached)
        
        
    def edit(self, priority, cached=False):
        req = self.requests['edit']
        pid = priority['id']
        
        page = req.GET('/secure/admin/EditPriority!default.jspa?id=' + pid).text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/required.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/toolbar/aui-toolbar-button-active-bg.png')

        token = extract(page, self.patterns['atl_token'])
                
        req.POST('/secure/admin/EditPriority.jspa',
            (
                NVPair('atl_token', token),
                NVPair('name', valueOrDefault(priority, 'name', extract(page, self.patterns['edit_name']))),
                NVPair('description', valueOrDefault(priority, 'description', extract(page, self.patterns['edit_description']))),
                NVPair('iconurl', valueOrDefault(priority, 'iconurl', extract(page, self.patterns['edit_iconurl']))),
                NVPair('statusColor', valueOrDefault(priority, 'statusColor', extract(page, self.patterns['edit_color']))),
                NVPair('Update', 'Update'),
                NVPair('id', pid),
                NVPair('preview', 'false'),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))                
        
        self.browse()
        