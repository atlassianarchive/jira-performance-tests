from HTTPClient import NVPair 
from env import request, extract, valueOrDefault
from java.util.regex import Pattern

class GeneralConfigAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view general configuration'),
            'edit' : request(testId + 1, 'HTTP-REQ : edit general configuration')
        }
        self.patterns = {
            'edit_title' : Pattern.compile('(?s)name="title".*?value="(.*?)"'),
            'edit_mode' : Pattern.compile('(?s)id="mode_select".*value="(.*?)" SELECTED'),
            'edit_captcha' : Pattern.compile('value="(.*?)" name="captcha".*?checked'),
            'edit_baseURL' : Pattern.compile('(?s)name="baseURL".*?value="(.*?)"'),
            'edit_emailFromHeaderFormat' : Pattern.compile('(?s)name="emailFromHeaderFormat".*?value="(.*?)"'),
            'edit_introduction' : Pattern.compile('(?s)name="introduction".*?\>(.*?)\<'),
            'edit_language' : Pattern.compile('(?s)name="language".*?value="(.*?)" SELECTED'),
            'edit_voting' : Pattern.compile('value="(.*?)" name="voting".*?checked'),
            'edit_watching' : Pattern.compile('value="(.*?)" name="watching".*?checked'),
            'edit_allowUnassigned' : Pattern.compile('value="(.*?)" name="allowUnassigned".*?checked'),
            'edit_externalUM' : Pattern.compile('value="(.*?)" name="externalUM".*?checked'),
            'edit_logoutConfirm' : Pattern.compile('value="(.*?)".*?name="logoutConfirm" checked'),
            'edit_useGzip' : Pattern.compile('value="(.*?)" name="useGzip".*?checked'),
            'edit_allowRpc' : Pattern.compile('value="(.*?)" name="allowRpc".*?checked'),
            'edit_emailVisibility' : Pattern.compile('value="([a-z]*?)".*?name="emailVisibility" checked'),
            'edit_groupVisibility' : Pattern.compile('value="([a-z]*?)".*?name="groupVisibility" checked'),
            'edit_excludePrecedenceHeader' : Pattern.compile('value="(.*?)" name="excludePrecedenceHeader".*?checked'),
            'edit_ajaxIssuePicker' : Pattern.compile('value="(.*?)" name="ajaxIssuePicker".*?checked'),
            'edit_ajaxUserPicker' : Pattern.compile('value="(.*?)" name="ajaxUserPicker".*?checked'),
            'edit_jqlAutocompleteDisabled' : Pattern.compile('value="(.*?)" name="jqlAutocompleteDisabled".*?checked'),
            'edit_showContactAdministratorsForm' : Pattern.compile('value="(.*?)" name="showContactAdministratorsForm".*?checked'),
            'edit_ieMimeSniffer' : Pattern.compile('(?s)name="ieMimeSniffer".*?value="([a-z]*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']

        req.GET('/secure/admin/ViewApplicationProperties.jspa')

        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            
        
    def edit(self, config, cached=False):
        req = self.requests['edit']
        
        page = req.GET('/secure/admin/EditApplicationProperties!default.jspa').text

        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/ico_preview.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
                
        token = extract(page, self.patterns['atl_token'])
                
        req.POST('/secure/admin/EditApplicationProperties.jspa',
            (
                NVPair('atl_token', token),
                NVPair('maximumAuthenticationAttemptsAllowed', '3'),
                NVPair('title', valueOrDefault(config, 'title', extract(page, self.patterns['edit_title']))),
                NVPair('mode', valueOrDefault(config, 'mode', extract(page, self.patterns['edit_mode']))),
                NVPair('captcha', valueOrDefault(config, 'captcha', extract(page, self.patterns['edit_captcha']))),
                NVPair('baseURL', valueOrDefault(config, 'baseURL', extract(page, self.patterns['edit_baseURL']))),
                NVPair('emailFromHeaderFormat', valueOrDefault(config, 'emailFromHeaderFormat', extract(page, self.patterns['edit_emailFromHeaderFormat']))),
                NVPair('introduction', valueOrDefault(config, 'introduction', extract(page, self.patterns['edit_introduction']))),
                NVPair('language', valueOrDefault(config, 'language', extract(page, self.patterns['edit_language']))),
                NVPair('defaultLocale', '-1'),
                NVPair('voting', valueOrDefault(config, 'voting', extract(page, self.patterns['edit_voting']))),
                NVPair('watching', valueOrDefault(config, 'watching', extract(page, self.patterns['edit_watching']))),
                NVPair('allowUnassigned', valueOrDefault(config, 'allowUnassigned', extract(page, self.patterns['edit_allowUnassigned']))),
                NVPair('externalUM', valueOrDefault(config, 'externalUM', extract(page, self.patterns['edit_externalUM']))),
                NVPair('logoutConfirm', valueOrDefault(config, 'logoutConfirm', extract(page, self.patterns['edit_logoutConfirm']))),
                NVPair('useGzip', valueOrDefault(config, 'useGzip', extract(page, self.patterns['edit_useGzip']))),
                NVPair('allowRpc', valueOrDefault(config, 'allowRpc', extract(page, self.patterns['edit_allowRpc']))),
                NVPair('emailVisibility', valueOrDefault(config, 'emailVisibility', extract(page, self.patterns['edit_emailVisibility']))),
                NVPair('groupVisibility', valueOrDefault(config, 'groupVisibility', extract(page, self.patterns['edit_groupVisibility']))),
                NVPair('excludePrecedenceHeader', valueOrDefault(config, 'excludePrecedenceHeader', extract(page, self.patterns['edit_excludePrecedenceHeader']))),
                NVPair('ajaxIssuePicker', valueOrDefault(config, 'ajaxIssuePicker', extract(page, self.patterns['edit_ajaxIssuePicker']))),
                NVPair('ajaxUserPicker', valueOrDefault(config, 'ajaxUserPicker', extract(page, self.patterns['edit_ajaxUserPicker']))),
                NVPair('jqlAutocompleteDisabled', valueOrDefault(config, 'jqlAutocompleteDisabled', extract(page, self.patterns['edit_jqlAutocompleteDisabled']))),
                NVPair('ieMimeSniffer', valueOrDefault(config, 'ieMimeSniffer', extract(page, self.patterns['edit_ieMimeSniffer']))),
                NVPair('showContactAdministratorsForm', valueOrDefault(config, 'showContactAdministratorsForm', extract(page, self.patterns['edit_showContactAdministratorsForm']))),
                NVPair('contactAdministratorsMessage', ''),
                NVPair('Update', 'Update'),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
        
        self.browse(cached)
