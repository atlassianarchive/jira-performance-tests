from HTTPClient import NVPair
from env import request, valueOrEmpty, extract
from java.util.regex import Pattern

class IssueNavigator:
    
    def __init__(self, testIndex):
        self.requests = {
            'simple_search' : request(testIndex, 'HTTP-REQ : issue navigator simple search'),
            'advanced_search' : request(testIndex + 1, 'HTTP-REQ : issue navigator advanced search')
        }
        self.patterns = {
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')                         
        }
        
    def simpleSearch(self, query={ 'pid' : '-1' }, cached=False):
        req = self.requests['simple_search']
        
        page = req.GET('/secure/IssueNavigator.jspa?mode=show&createNew=true').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/includes/jira/issue/searchIssueTypes.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/permalink_light_16.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_filtercollapse.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/tab_bg.png')
            req.GET('/images/icons/ico_help.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_blockcollapsed.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_descending.png')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/ico_activeissue.png')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/images/icons/status_inprogress.gif')
            req.GET('/images/icons/status_resolved.gif')
            req.GET('/images/icons/status_closed.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-icon-forms.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
                
        req.POST('/secure/IssueNavigator.jspa',
            (
                NVPair('atl_token', extract(page, self.patterns['atl_token'])),
                NVPair('reset', 'update'),
                NVPair('query', valueOrEmpty(query, 'query')),
                NVPair('summary', valueOrEmpty(query, 'summary')),
                NVPair('description', valueOrEmpty(query, 'description')),
                NVPair('body', valueOrEmpty(query, 'body')),
                NVPair('environment', valueOrEmpty(query, 'environment')),
                NVPair('pid', valueOrEmpty(query, 'pid')),
                NVPair('type', valueOrEmpty(query, 'type')),
                NVPair('status', valueOrEmpty(query, 'status')),
                NVPair('resolution', valueOrEmpty(query, 'resolution')),
                NVPair('priority', valueOrEmpty(query, 'priority')),
                NVPair('labels', valueOrEmpty(query, 'labels')),
                NVPair('created:after', valueOrEmpty(query, 'created:after')),
                NVPair('created:before', valueOrEmpty(query, 'created:before')),
                NVPair('created:previous', valueOrEmpty(query, 'created:previous')),
                NVPair('created:next', valueOrEmpty(query, 'created:next')),
                NVPair('updated:after', valueOrEmpty(query, 'updated:after')),
                NVPair('updated:before', valueOrEmpty(query, 'updated:before')),
                NVPair('updated:previous', valueOrEmpty(query, 'updated:previous')),
                NVPair('updated:next', valueOrEmpty(query, 'updated:next')),
                NVPair('duedate:after', valueOrEmpty(query, 'duedate:after')),
                NVPair('duedate:before', valueOrEmpty(query, 'duedate:before')),
                NVPair('duedate:previous', valueOrEmpty(query, 'duedate:previous')),
                NVPair('duedate:next', valueOrEmpty(query, 'duedate:next')),
                NVPair('resolutiondate:after', valueOrEmpty(query, 'resolutiondate:after')),
                NVPair('resolutiondate:before', valueOrEmpty(query, 'resolutiondate:before')),
                NVPair('resolutiondate:previous', valueOrEmpty(query, 'resolutiondate:previous')),
                NVPair('resolutiondate:next', valueOrEmpty(query, 'resolutiondate:next')),
                NVPair('workratio:min', valueOrEmpty(query, 'workratio:min')),
                NVPair('workratio:max', valueOrEmpty(query, 'workratio:max')),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))

        
    def advancedSearch(self, jql, cached=False):
        req = self.requests['advanced_search']

        req.GET('/secure/IssueNavigator!executeAdvanced.jspa')
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jqlautocomplete/jira.webresources:jqlautocomplete.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/permalink_light_16.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_filtercollapse.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/tab_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/ico_activeissue.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_descending.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/accept.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
                
        req.POST('/secure/IssueNavigator!executeAdvanced.jspa',
                (                
                    NVPair('jqlQuery', jql),
                    NVPair('runQuery', 'true'),
                ), ( 
                    NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
                ))
