from net.grinder.plugin.http import HTTPPluginControl
from net.grinder.script import Test
from HTTPClient import NVPair
from interactions import *

connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started

connectionDefaults.defaultHeaders = \
  [ NVPair('Accept-Language', 'en-US,en;q=0.8'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'),
    NVPair('Accept-Encoding', 'gzip,deflate,sdch'),
    NVPair('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'),
    NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'), ]

FILTER_SEARCHES = 30
DASHBOARD_VIEWS = 15
REPORT_VIEWS = 20
ISSUE_COUNT_COMMENT = 5
ISSUE_COUNT_VIEW = 10
ISSUE_COUNT_TRANSITION = 3
ISSUE_COUNT_EDIT = 2
PROJECT_VIEWS = 10

class TestRunner:
    
    def __call__(self):
        goHome()
        doLogin()
        useSavedFilter(FILTER_SEARCHES)
        viewDashboard(DASHBOARD_VIEWS)
        viewReports(REPORT_VIEWS)
        addComments(ISSUE_COUNT_COMMENT)
        browseIssues(ISSUE_COUNT_VIEW)
        transitionIssues(ISSUE_COUNT_TRANSITION)
        # editIssues(ISSUE_COUNT_EDIT)
        browseProjects(PROJECT_VIEWS)

Test(1, 'view dashboard - not logged in').wrap(goHome)
Test(2, 'login & view dashboard').wrap(doLogin)
Test(3, 'view ' + str(ISSUE_COUNT_VIEW) + ' issues').wrap(browseIssues)
Test(5, 'add comment to ' + str(ISSUE_COUNT_COMMENT) + ' issues').wrap(addComments)
Test(6, 'do ' + str(ISSUE_COUNT_TRANSITION) + ' workflow transitions').wrap(transitionIssues)
Test(8, 'do ' + str(FILTER_SEARCHES) + ' searches via saved filters').wrap(useSavedFilter)
Test(9, 'view ' + str(PROJECT_VIEWS) + ' projects with random tabs').wrap(browseProjects)
# Test(11, 'edit ' + str(ISSUE_COUNT_EDIT) + ' issues').wrap(editIssues)
Test(12, 'view dashboard ' + str(DASHBOARD_VIEWS) + ' times').wrap(viewDashboard)
Test(13, 'view ' + str(REPORT_VIEWS) + ' reports').wrap(viewReports)
