from HTTPClient import NVPair
from env import request, cacheRequest, valueOrEmpty, extract
from java.util.regex import Pattern

class IssueNavigator:
    
    def __init__(self, testIndex):
        cacheIndex = testIndex * 100
        self.requests = {
            'issuenav' : request(testIndex, 'HTTP-REQ : issue navigator'),
            'simple_search' : request(testIndex + 1, 'HTTP-REQ : issue navigator simple search'),
            'advanced_search' : request(testIndex + 2, 'HTTP-REQ : issue navigator advanced search'),
            'background_load' : request(testIndex + 3, 'HTTP-REQ : issue navigator background requests'),
            'issuenav_cache' : cacheRequest(cacheIndex, 'CACHE : issue navigator'),
        }
        self.patterns = {
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')                         
        }
        
    def simpleSearch(self, query={ 'pid' : '-1' }, cached=False):
        req = self.requests['issuenav']
        searchReq = self.requests['simple_search']
        cacheReq = self.requests['issuenav_cache']
        backgroundReq = self.requests['background_load']

        page = req.GET('/issues/?jql=').text
        
        if not cached:
            cacheReq.GET('/issues/')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.0-m10/_/download/batch/com.atlassian.auiplugin:aui-experimental-tooltips/com.atlassian.auiplugin:aui-experimental-tooltips.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/401edc1b3afdc4befba197d952455e40/_/download/contextbatch/css/jira.global/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/86d51fa24f766a078f46bfefc713bc60/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-form/com.atlassian.jira.jira-quick-edit-plugin:quick-form.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/c03802a4af1811c54fcadfd5e15918af/_/download/contextbatch/css/jira.general/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:viewissue/com.atlassian.jira.jira-issue-nav-plugin:viewissue.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:old_header_resources/jira.webresources:old_header_resources.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:common/com.atlassian.jira.jira-issue-nav-plugin:common.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-share-plugin:share-resources/com.atlassian.jira.jira-share-plugin:share-resources.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.3.11/_/download/batch/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.1/_/download/batch/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/c03802a4af1811c54fcadfd5e15918af/_/download/contextbatch/js/jira.general/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.0-m10/_/download/batch/com.atlassian.auiplugin:aui-experimental-tooltips/com.atlassian.auiplugin:aui-experimental-tooltips.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.6/_/download/batch/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jquery-easing/jira.webresources:jquery-easing.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/86d51fa24f766a078f46bfefc713bc60/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/401edc1b3afdc4befba197d952455e40/_/download/contextbatch/js/jira.global/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:group-pickers/jira.webresources:group-pickers.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:user-pickers/jira.webresources:user-pickers.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-form/com.atlassian.jira.jira-quick-edit-plugin:quick-form.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-edit-issue/com.atlassian.jira.jira-quick-edit-plugin:quick-edit-issue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jqlautocomplete/jira.webresources:jqlautocomplete.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-searchheader/com.atlassian.jira.jira-issue-nav-plugin:issuenav-searchheader.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2/_/download/batch/com.atlassian.jira.jira-share-plugin:share-resources/com.atlassian.jira.jira-share-plugin:share-resources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:backbone-ext/com.atlassian.jira.jira-issue-nav-plugin:backbone-ext.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:common/com.atlassian.jira.jira-issue-nav-plugin:common.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-filter/com.atlassian.jira.jira-issue-nav-plugin:issuenav-filter.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-table/com.atlassian.jira.jira-issue-nav-plugin:issuenav-table.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-jql/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-jql.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-common/com.atlassian.jira.jira-issue-nav-plugin:issuenav-common.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:viewissue/com.atlassian.jira.jira-issue-nav-plugin:viewissue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-basic/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-basic.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:searchers/jira.webresources:searchers.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/3.2.0-m2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav/com.atlassian.jira.jira-issue-nav-plugin:issuenav.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/3.2.0-m2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.3.11/_/download/batch/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.1/_/download/batch/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:standalone-issue-nav/com.atlassian.jira.jira-issue-nav-plugin:standalone-issue-nav.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-create-issue/com.atlassian.jira.jira-quick-edit-plugin:quick-create-issue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.css')
            cacheReq.GET('/rest/api/1.0/shortcuts/809/b6e558e79b363889b33280eb330fee2d/shortcuts.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/3.2.0-m2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/images/icons/improvement.gif')
            cacheReq.GET('/images/icons/priority_trivial.gif')
            cacheReq.GET('/images/icons/sales.gif')
            cacheReq.GET('/images/icons/status_generic.gif')
            cacheReq.GET('/images/icons/genericissue.gif')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/_/images/icon-jira-logo.png')
            cacheReq.GET('/rest/helptips/1.0/tips')
            cacheReq.GET('/images/icons/priority_critical.gif')
            cacheReq.GET('/images/icons/status_open.gif')
            cacheReq.GET('/images/icons/priority_major.gif')
            cacheReq.GET('/images/icons/task.gif')
            cacheReq.GET('/images/icons/status_inprogress.gif')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/images/icons/status_resolved.gif')
            cacheReq.GET('/images/icons/exclamation.gif')
            cacheReq.GET('/images/icons/priority_blocker.gif')
            cacheReq.GET('/images/icons/status_closed.gif')
            cacheReq.GET('/images/icons/issue_subtask.gif')
            cacheReq.GET('/images/icons/status_reopened.gif')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/images/icons/ico_epic.png')
            cacheReq.GET('/images/icons/priority_minor.gif')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/permalink_light_16.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/icon_separator.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/sprites/icons_module.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/view_20.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/tools_20.png')
            cacheReq.GET('/rest/api/2/filter/favourite')
            cacheReq.GET('/secure/SetSelectedIssue.jspa')


        # get the XSRF token
        atlassianToken = extract(page, self.patterns['atl_token'])
        # Click on Issue Type drop-down
        searchReq.GET('/secure/SearchRendererEdit!Default.jspa?fieldId=issuetype&decorator=none&jqlContext=&_=1350276571520').text
        searchReq.GET('/secure/Search!Default.jspa?decorator=none&type=1&_=1350276573831').text
        # Don't need to wait on these results because we don't care
        backgroundReq.GET('/rest/issueNav/1/issueTable/?jql=issuetype+%3D+Bug&useUserColumns=true&_=1350276574996').text
        backgroundReq.GET('/secure/SetSelectedIssue.jspa?atl_token=' + atlassianToken + '&selectedIssueId=407945&selectedIssueIndex=0&nextIssueId=407833&_=1350276576076').text
        # Click on Status drop-down
        searchReq.GET('/secure/SearchRendererEdit!Default.jspa?fieldId=status&decorator=none&jqlContext=issuetype+%3D+Bug&_=1350276765547').text
        searchReq.GET('/secure/Search!Default.jspa?decorator=none&type=1&status=1&_=1350276826873').text
        # We do need to wait on these results - these are the final search results
        searchReq.GET('/rest/issueNav/1/issueTable/?jql=issuetype+%3D+Bug+AND+status+%3D+Open&useUserColumns=true&_=1350276828034').text
        backgroundReq.GET('/secure/SetSelectedIssue.jspa?atl_token=' + atlassianToken + '&selectedIssueId=407945&selectedIssueIndex=0&nextIssueId=407446&_=1350276829110').text

    def advancedSearch(self, jql, cached=False):
        req = self.requests['issuenav']
        searchReq = self.requests['advanced_search']
        cacheReq = self.requests['issuenav_cache']
        backgroundReq = self.requests['background_load']


        page = req.GET('/issues/?jql=').text
        
        if not cached:
            cacheReq.GET('/issues/')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.0-m10/_/download/batch/com.atlassian.auiplugin:aui-experimental-tooltips/com.atlassian.auiplugin:aui-experimental-tooltips.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/401edc1b3afdc4befba197d952455e40/_/download/contextbatch/css/jira.global/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/86d51fa24f766a078f46bfefc713bc60/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-form/com.atlassian.jira.jira-quick-edit-plugin:quick-form.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/c03802a4af1811c54fcadfd5e15918af/_/download/contextbatch/css/jira.general/batch.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:viewissue/com.atlassian.jira.jira-issue-nav-plugin:viewissue.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:old_header_resources/jira.webresources:old_header_resources.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:common/com.atlassian.jira.jira-issue-nav-plugin:common.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2/_/download/batch/com.atlassian.jira.jira-share-plugin:share-resources/com.atlassian.jira.jira-share-plugin:share-resources.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.3.11/_/download/batch/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.1/_/download/batch/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/c03802a4af1811c54fcadfd5e15918af/_/download/contextbatch/js/jira.general/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.0-m10/_/download/batch/com.atlassian.auiplugin:aui-experimental-tooltips/com.atlassian.auiplugin:aui-experimental-tooltips.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.6/_/download/batch/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib/com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jquery-easing/jira.webresources:jquery-easing.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/86d51fa24f766a078f46bfefc713bc60/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/401edc1b3afdc4befba197d952455e40/_/download/contextbatch/js/jira.global/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:group-pickers/jira.webresources:group-pickers.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:user-pickers/jira.webresources:user-pickers.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-form/com.atlassian.jira.jira-quick-edit-plugin:quick-form.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-edit-issue/com.atlassian.jira.jira-quick-edit-plugin:quick-edit-issue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:jqlautocomplete/jira.webresources:jqlautocomplete.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-searchheader/com.atlassian.jira.jira-issue-nav-plugin:issuenav-searchheader.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2/_/download/batch/com.atlassian.jira.jira-share-plugin:share-resources/com.atlassian.jira.jira-share-plugin:share-resources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:backbone-ext/com.atlassian.jira.jira-issue-nav-plugin:backbone-ext.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:common/com.atlassian.jira.jira-issue-nav-plugin:common.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-filter/com.atlassian.jira.jira-issue-nav-plugin:issuenav-filter.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-table/com.atlassian.jira.jira-issue-nav-plugin:issuenav-table.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-jql/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-jql.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-common/com.atlassian.jira.jira-issue-nav-plugin:issuenav-common.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:viewissue/com.atlassian.jira.jira-issue-nav-plugin:viewissue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-basic/com.atlassian.jira.jira-issue-nav-plugin:issuenav-query-basic.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:searchers/jira.webresources:searchers.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/3.2.0-m2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:issuenav/com.atlassian.jira.jira-issue-nav-plugin:issuenav.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/3.2.0-m2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.3.11/_/download/batch/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts/com.atlassian.plugins.atlassian-nav-links-plugin:rotp-projectshortcuts.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.1/_/download/batch/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources/com.atlassian.administration.atlassian-admin-quicksearch-jira:admin-quicksearch-webresources.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.1.22/_/download/batch/com.atlassian.jira.jira-issue-nav-plugin:standalone-issue-nav/com.atlassian.jira.jira-issue-nav-plugin:standalone-issue-nav.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0.54/_/download/batch/com.atlassian.jira.jira-quick-edit-plugin:quick-create-issue/com.atlassian.jira.jira-quick-edit-plugin:quick-create-issue.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/5.2-SNAPSHOT/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.css')
            cacheReq.GET('/rest/api/1.0/shortcuts/809/b6e558e79b363889b33280eb330fee2d/shortcuts.js')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/3.2.0-m2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/images/icons/improvement.gif')
            cacheReq.GET('/images/icons/priority_trivial.gif')
            cacheReq.GET('/images/icons/sales.gif')
            cacheReq.GET('/images/icons/status_generic.gif')
            cacheReq.GET('/images/icons/genericissue.gif')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/_/images/icon-jira-logo.png')
            cacheReq.GET('/rest/helptips/1.0/tips')
            cacheReq.GET('/images/icons/priority_critical.gif')
            cacheReq.GET('/images/icons/status_open.gif')
            cacheReq.GET('/images/icons/priority_major.gif')
            cacheReq.GET('/images/icons/task.gif')
            cacheReq.GET('/images/icons/status_inprogress.gif')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/images/icons/status_resolved.gif')
            cacheReq.GET('/images/icons/exclamation.gif')
            cacheReq.GET('/images/icons/priority_blocker.gif')
            cacheReq.GET('/images/icons/status_closed.gif')
            cacheReq.GET('/images/icons/issue_subtask.gif')
            cacheReq.GET('/images/icons/status_reopened.gif')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/images/icons/ico_epic.png')
            cacheReq.GET('/images/icons/priority_minor.gif')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/permalink_light_16.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/icon_separator.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/sprites/icons_module.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/view_20.png')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/s/en_AUayl91x-418945332/809/3/1.0/_/images/icons/tools_20.png')
            cacheReq.GET('/rest/api/2/filter/favourite')
            cacheReq.GET('/secure/SetSelectedIssue.jspa')

        # The request method is smart enough to URL escape the required chars from the string.
        searchReq.GET('/rest/issueNav/1/issueTable/?jql=' + jql + '&useUserColumns=false&_=1231239313666')
        backgroundReq.GET('/secure/Search!Jql.jspa?decorator=none&jql=' + jql + '&_=1231239313666')
        # get the XSRF token
        atlassianToken = extract(page, self.patterns['atl_token'])
        backgroundReq.GET('/secure/SetSelectedIssue.jspa?atl_token=' + atlassianToken + '&selectedIssueId=409993&selectedIssueIndex=0&nextIssueId=409988&_=1350279314931')
