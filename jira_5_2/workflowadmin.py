from HTTPClient import NVPair
from env import request, extract, valueOrDefault, valueOrEmpty, randChoice
from java.util.regex import Pattern

class WorkflowAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view workflows'),
            'add' : request(testId + 1, 'HTTP-REQ : add workflow'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit workflow'),
            'view' : request(testId + 3, 'HTTP-REQ : view workflow designer')
        }
        self.patterns = {
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"'),
            'atl_token_dialog' : Pattern.compile('(?s)name="atl_token".*?value="(.*?)"'),
            'workflow_name' : Pattern.compile('workflowName=(.*?)')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/workflows/ListWorkflows.jspa').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            
        return extract(page, self.patterns['atl_token'])
        
        
    def add(self, workflow, cached=False):
        req = self.requests['add']
        name = workflow['newWorkflowName']

        dialog = req.GET('/secure/admin/workflows/AddNewWorkflow.jspa?inline=true&decorator=dialog&_=1329953491086').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-active-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
                
        req.POST('/secure/admin/workflows/AddWorkflow.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('newWorkflowName', name),
            NVPair('description', valueOrEmpty(workflow, 'description')),
            NVPair('atl_token', extract(dialog, self.patterns['atl_token_dialog'])),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        self.view(name, cached)
        
    def getRandomWorkflowName(self,req):
         page = req.GET('/secure/admin/workflows/ListWorkflows.jspa').text
         edits = Pattern.compile('id="edit_live.*?workflowName=(.*?)">Edit')
         m = edits.matcher(page)
         names = []
         while(m.find()):
             names.append(m.group(1))
         return randChoice(names)

    def edit(self, token, workflow, cached=False):
        req = self.requests['edit']
        name = self.getRandomWorkflowName(req)
        dialog = page = req.GET('/secure/admin/workflows/EditWorkflow!default.jspa?atl_token=' + token + '&workflowMode=live&workflowName=' + name + '&inline=true&decorator=dialog').text
        token = extract(dialog, self.patterns['atl_token_dialog'])
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
                
        req.POST('/secure/admin/workflows/EditWorkflow.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('newWorkflowName', valueOrDefault(workflow, 'newWorkflowName', name)),
            NVPair('description', valueOrDefault(workflow, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('atl_token', token),
            NVPair('workflowName', name),
            NVPair('workflowMode', 'live'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
    
        return self.view(name, cached)
    

    def view(self, name, cached=False):
        req = self.requests['view']
        
        req.GET('/secure/admin/workflows/WorkflowDesigner.jspa?wfName=' + name + '&workflowMode=live')
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-4qg4om/732/5/5.0.1-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/com.atlassian.jira.jira-project-config-plugin:project-config-global.css')
            req.GET('/s/en_US-4qg4om/732/5/2.5.4/_/download/batch/com.atlassian.jira.plugins.jira-workflow-designer:jwdcss/com.atlassian.jira.plugins.jira-workflow-designer:jwdcss.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/2.5.4/_/download/batch/com.atlassian.jira.plugins.jira-workflow-designer:jwdresources/com.atlassian.jira.plugins.jira-workflow-designer:jwdresources.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-4qg4om/732/5/5.0.1-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-project-config-plugin:project-config-global/com.atlassian.jira.jira-project-config-plugin:project-config-global.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/2.5.4/_/download/batch/com.atlassian.jira.plugins.jira-workflow-designer:jwd-topup/com.atlassian.jira.plugins.jira-workflow-designer:jwd-topup.js')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-workflows.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/_/download/resources/com.atlassian.jira.plugins.jira-workflow-designer:workflowdesignerflex/flex/jira-workflow-designer-flex-2.5.4.swf')
            req.GET('/download/resources/com.atlassian.jira.plugins.jira-workflow-designer:topupImages/images/top_up/loader.gif')        
