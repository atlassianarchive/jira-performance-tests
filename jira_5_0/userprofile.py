from env import request, extract, extractAll
from java.util.regex import Pattern

class UserProfile:
    
    def __init__(self, testIndex):
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view user profile')
        }
        self.patterns = {
            'project_avatars' : Pattern.compile('img class="project-avatar.*?src="(.*?)"'),     
            'user_avatar' : Pattern.compile('(?s)id="user_avatar_image".*?src="(.*?)"'),
            'stream' : Pattern.compile('iframe id="gadget-1".*?src="http:\/\/.*?\/(.*?)"')
        }
        
    def view(self, username, cached=False):
        req = self.requests['view']

        page = req.GET('/secure/ViewProfile.jspa').text
        req.GET(extract(page, self.patterns['stream']))
        req.GET('/rest/activity-stream/1.0/preferences')
        req.GET('/plugins/servlet/streams?maxResults=10&streams=user+IS+' + username)
        req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328667180751')
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:avatar-picker/jira.webresources:avatar-picker.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:manageshared/jira.webresources:manageshared.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:userprofile/jira.webresources:userprofile.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:avatar-picker/jira.webresources:avatar-picker.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/ico_filters.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/s/en_USkppxta/712/7/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_USkppxta/712/7/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streamsGadgetResources/com.atlassian.streams:streamsGadgetResources.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/tools_12.png')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')
            req.GET('/images/icons/bug.gif')
                    
            req.GET(extract(page, self.patterns['user_avatar']))
            for projectAvatar in extractAll(page, self.patterns['project_avatars']):
                req.GET(projectAvatar)
        