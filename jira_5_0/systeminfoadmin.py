from env import request

class SystemInfoAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : system info')
        }
        
    def browse(self, cached=False):
        req = self.requests['browse']
        
        req.GET('/secure/admin/jira/ViewSystemInfo.jspa')
        
        if not cached:
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/ico_help.png')
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/en_USkppxta/713/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/new/icon18-charlie.png')
