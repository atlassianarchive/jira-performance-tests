from HTTPClient import NVPair
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class PermissionSchemeAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : browse permission schemes'),
            'add' : request(testId + 1, 'HTTP-REQ : add permission scheme'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit permission scheme')
        }
        self.patterns = {
            'newSchemeId' : Pattern.compile('schemeId=([0-9]*)'),
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?\>(.*?)\<'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }


    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewPermissionSchemes.jspa').text
        
        if not cached:
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_USkppxta/713/4/_/images/icons/ico_help.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_USkppxta/713/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/new/icon18-charlie.png')
                    
        return extract(page, self.patterns['atl_token'])

        
    def add(self, scheme, cached=False):
        req = self.requests['add']
        
        page = req.GET('/secure/admin/AddPermissionScheme!default.jspa').text
        
        if not cached:
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/713/4/_/images/icons/ico_help.png')
            req.GET('/s/en_USkppxta/713/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/new/icon18-charlie.png')

        token = extract(page, self.patterns['atl_token'])
                
        response = req.POST('/secure/admin/AddPermissionScheme.jspa',
            (
                NVPair('atl_token', token),
                NVPair('name', scheme['name']),
                NVPair('description', valueOrEmpty(scheme, 'description')),
                NVPair('Add', 'Add'),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
        newSchemeId = extract(response.getHeader('Location'), self.patterns['newSchemeId'])

        req.GET('/secure/admin/ViewPermissionSchemes!default.jspa?schemeId=' + newSchemeId)

        return newSchemeId 
        
    
    def edit(self, scheme, cached=False):
        req = self.requests['edit']
        sid = scheme['id']
            
        page = req.GET('/secure/admin/EditPermissionScheme!default.jspa?schemeId=' + sid).text
                
        if not cached:
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/713/4/_/images/jira111x30.png')

        token = extract(page, self.patterns['atl_token'])
                
        req.POST('/secure/admin/EditPermissionScheme.jspa',
            (
                NVPair('atl_token', token),
                NVPair('name', valueOrDefault(scheme, 'name', extract(page, self.patterns['edit_name']))),
                NVPair('description', valueOrDefault(scheme, 'description', extract(page, self.patterns['edit_description']))),
                NVPair('Update', 'Update'),
                NVPair('schemeId', sid),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))                
                
        self.browse(cached)
