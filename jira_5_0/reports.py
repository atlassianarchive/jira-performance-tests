from env import request, extract
from java.util.regex import Pattern

class Reports:
    
    def __init__(self, testId):
        self.requests = {
            'configure' : request(testId, 'HTTP REQ : configure report'),
            'report' : request(testId + 1, 'HTTP REQ : view report')
        }
        self.patterns = {
            'chart' : Pattern.compile('(\/charts\?filename=jfreechart-onetime-.*?\.png)')
        }
        
    def viewCreatedVsResolved(self, projectId, cached=False):
        req = self.requests['configure']
        
        req.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report')
        
        if not cached:
            req.GET('/s/en_USkppxta/712/3/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/3/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/3/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/3/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_USkppxta/712/3/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/3/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/3/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/3/_/images/jira111x30.png')

        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        if not cached:
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
                
        req = self.requests['report']
        page = req.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&cumulative=true&versionLabels=major&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report&Next=Next').text

        if self.patterns['chart'].matcher(page).find():
            req.GET(extract(page, self.patterns['chart']))

        
    def viewRecentlyCreatedIssues(self, projectId, cached=False):
        req = self.requests['configure']
        
        req.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report')
        
        if not cached:
            req.GET('/s/en_USkppxta/712/3/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/3/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/3/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/3/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_USkppxta/712/3/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/3/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/3/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/3/_/images/jira111x30.png')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_USkppxta/712/3/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
        
        req = self.requests['report']        
        page = self.requests['report'].GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report&Next=Next').text
        if self.patterns['chart'].matcher(page).find():
            req.GET(extract(page, self.patterns['chart']))
