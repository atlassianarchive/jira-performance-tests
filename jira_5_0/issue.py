from HTTPClient import NVPair
from env import request, extract, valueOrEmpty, valueOrDefault, postMultipart, extractAll
from java.util.regex import Pattern
from java.lang import String
from form import extractOptions, extractSelects, extractSubmit, extractTextAreas, extractTextfields

class Issue:
    
    def __init__(self, testIndex, issueKey, issueId):
        self.issueKey = issueKey
        self.issueId = issueId
        self.projectKey = String(issueKey).split("-")[0]
        
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view issue'),
            'add_comment' : request(testIndex + 1, 'HTTP-REQ : add comment to issue'),
            'edit' : request(testIndex + 2, 'HTTP-REQ : edit issue'),
            'workflow' : request(testIndex + 3, 'HTTP-REQ : workflow')
        }
        self.patterns = {
            'stream' : Pattern.compile('id="gadget-stream.*?src="http:\/\/.*?(\/.*?)"'),
            'tab_activity_stream' : Pattern.compile('id="gadget-0".*?src="http:\/\/.*?(\/.*?)"'),
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"'),
            'user_avatars' : Pattern.compile('background-image:url\((\/secure\/useravatar?.*?)\)'),
            'edit_user_avatars' : Pattern.compile('(/secure/useravatar.*?)\''),
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"'),
            'edit_summary' : Pattern.compile('(?s)id=\\\\"summary\\\\".*?value=\\\\"(.*?)\\\\"'),
            'edit_issuetype' : Pattern.compile('(?s)id=\\\\"issuetype\\\\".*?selected=\\\\"selected\\\\".*?value=\\\\"([0-9]*)\\\\"'),
            'edit_priority' : Pattern.compile('(?s)id=\\\\"priority\\\\".*?selected=\\\\"selected\\\\".*?value=\\\\"([0-9]*)\\\\"'),
            'edit_assignee' : Pattern.compile('(?s)id=\\\\"assignee\\\\".*?value=\\\\"(.*?)\\\\"'),
            'edit_description' : Pattern.compile('(?s)id=\\\\"description\\\\".*?\>(.*?)\<'),
            'edit_reporter' : Pattern.compile('(?s)id=\\\\"reporter\\\\".*?value=\\\\"(.*?)\\\\"'),
            'transition_assignee' : Pattern.compile('(?s)id="assignee".*?selected="selected".*?value="(.*?)"'),
            'workflow_action' : Pattern.compile('\/secure\/WorkflowUIDispatcher\.jspa.*?action=([0-9]*)'),
            'workflow_form' : Pattern.compile('(?s)form (action="\/secure\/CommentAssignIssue.*?)<\/form'),            
            'is_closed' : Pattern.compile('(?s)\/images\/icons\/status_closed.gif')               
        }


    def getKey(self):
        return self.issueKey
    

    def view(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.issueKey).text
        
        if not cached:
            req.GET('/s/en_USkppxta/712/4/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/4/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/button_bg.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/create_12.png')
            
            req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), ) ))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                req.GET(userAvatar)
                
        return { 
            'workflow_actions' : extractAll(page, self.patterns['workflow_action']),
            'editable' : not self.patterns['is_closed'].matcher(page).find(),
            'atl_token' : extract(page, self.patterns['atl_token'])
        }


    def nextWorkflowStep(self, atl_token, stepId, values={}, cached=False):
        req = self.requests['workflow']
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1/_/includes/jquery/plugins/fancybox/fancybox.png')

        response = req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=' + stepId + '&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328236631852')
        
        # workflow action without screen mutates on GET
        if response.getStatusCode() == 302:
            # extract all form data and construct POST request
            page = req.GET(response.getHeader('Location')).text
            
            form = extract(page, self.patterns['workflow_form'])
            params = []
            
            for select in extractSelects(form):
                name = select[0]
                if name != 'fixVersions':   #frother evasion
                    options = extractOptions(select[1])
                    params.append(NVPair(name, valueOrDefault(values, name, options[0])))
            
            for textfield in extractTextfields(form):
                params.append(NVPair(textfield, valueOrEmpty(values, textfield)))
                
            for textarea in extractTextAreas(form):
                if textarea != 'fixVersions': #more frother evasion
                    params.append(NVPair(textarea, valueOrEmpty(values, textarea)))
                
            params.append(NVPair(extractSubmit(form), ''))
            params.append(NVPair('action', stepId))
            params.append(NVPair('id', self.issueId))
            params.append(NVPair('atl_token', atl_token))
            
            req.POST('/secure/CommentAssignIssue.jspa', params, ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ))
        
        return self.view(cached)
    

    def startProgress(self, atl_token, cached=False):
        req = self.requests['start_progress']
        
        if not cached:
            req.GET('/s/en_USkppxta/712/4/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/images/icons/status_inprogress.gif')
           
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=4&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328236631852')
        return self.view(cached)
        

    def stopProgress(self, atl_token, cached=False):
        req = self.requests['stop_progress']

        if not cached:
            req.GET('/s/en_USkppxta/712/4/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/images/icons/status_open.gif')
        
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=301&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328237770980')
        return self.view(cached)
        

    def resolve(self, atl_token, cached=False):
        req = self.requests['resolve']
  
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=5&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328238748158')
        page = req.GET('/secure/CommentAssignIssue!default.jspa?action=5&inline=true&atl_token=' + atl_token + '&id=' + self.issueId + '&decorator=dialog&_=1328238748158').text

        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-icon-forms.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_US-64k3hp/664/2/1/_/includes/jquery/plugins/fancybox/fancybox.png')
        
        req.POST('/secure/CommentAssignIssue.jspa?atl_token=' + atl_token,
        (
            NVPair('action', '5'),
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('decorator', 'dialog'),
            NVPair('viewIssueKey', ''),
            NVPair('resolution', '1'),
            NVPair('assignee', extract(page, self.patterns['transition_assignee'])),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
        ),
        ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )

        return self.view(cached)


    def close(self, atl_token, cached=False):
        req = self.requests['close']
        
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=701&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328240402504')
        page = req.GET('/secure/CommentAssignIssue!default.jspa?decorator=dialog&atl_token=' + atl_token + '&_=1328240402504&id=' + self.issueId + '&action=701&inline=true').text

        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/wait.gif')
        
        req.POST('/secure/CommentAssignIssue.jspa?atl_token=' + atl_token,
        (
            NVPair('action', '701'),
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('viewIssueKey', ''),
            NVPair('assignee', extract(page, self.patterns['transition_assignee'])),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
        ),
        ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )

        return self.view(cached)
        
        
    def reopen(self, atl_token, cached=False):
        req = self.requests['reopen']
                
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=3&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328240851124')
        page = req.GET('/secure/CommentAssignIssue!default.jspa?id=' + self.issueId + '&atl_token=' + atl_token + '&_=1328240851124&action=3&decorator=dialog&inline=true').text

        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1/_/includes/jquery/plugins/fancybox/fancybox.png')    
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/wait.gif')
        
        req.POST('/secure/CommentAssignIssue.jspa?atl_token=' + atl_token,
        (
            NVPair('action', '3'),
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('viewIssueKey', ''),
            NVPair('assignee', extract(page, self.patterns['transition_assignee'])),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
        ),
        ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        
        return self.view(cached)
                
    def viewTabAll(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel').text

        if not cached:        
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/button_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
        
            req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                req.GET(userAvatar)
        
        
    def viewTabComments(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel').text
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/button_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/tab_bg.png')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
            
            req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))        

            
    def viewTabChangeHistory(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:changehistory-tabpanel').text
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/button_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
        
            req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                req.GET(userAvatar)
        

    def viewTabActivityStream(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.streams.streams-jira-plugin:activity-stream-issue-tab').text
        req.GET(extract(page, self.patterns['tab_activity_stream']))
        req.GET('/rest/activity-stream/1.0/preferences')
        req.GET('/plugins/servlet/streams?maxResults=20&streams=issue-key+IS+' + self.issueKey + '&streams=key+IS+' + self.projectKey)
        req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328655669854')
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/button_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/s/en_USkppxta/712/7/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            req.GET('/s/en_USkppxta/712/7/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/batch/com.atlassian.streams:streamsGadgetResources/com.atlassian.streams:streamsGadgetResources.js')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            req.GET('/s/en_USkppxta/712/7/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            req.GET('/s/en_USkppxta/712/7/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/tools_12.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')


    def addComment(self, atl_token, comment={ 'comment' : 'test' }, cached=False):
        req = self.requests['add_comment']
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/stalker-ext-grad.png')
                
        req.POST('/secure/AddComment.jspa?atl_token=' + atl_token,
        (
            NVPair('atl_token', atl_token),
            NVPair('id', self.issueId),
            NVPair('comment', valueOrEmpty(comment, 'comment')),
            NVPair('commentLevel', valueOrEmpty(comment, 'commentLevel')),
            NVPair('Add', 'Add'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))       


    def edit(self, atl_token, edit={}, cached=False):
        req = self.requests['edit']
        
        dialog = req.POST('/secure/QuickEditIssue!default.jspa?issueId=' + self.issueId + '&decorator=none').text
        
        if not cached:        
            req.GET('/images/icons/ico_help.png')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0.28/_/download/resources/com.atlassian.jira.jira-quick-edit-plugin:quick-form/images/icon-cog.png')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_minor.gif')
            
        req.POST('/rest/quickedit/1.0/userpreferences/edit',
                 '{\"useQuickForm\":false,\"fields\":[\"fixVersions\",\"assignee\",\"labels\",\"components\",\"priority\",\"comment\"],\"showWelcomeScreen\":true}',
                 ( NVPair('Content-Type', 'application/json'), )
            )

        if not cached:
            for userAvatar in extractAll(dialog, self.patterns['edit_user_avatars'], (('&amp;', '&'), )):
                req.GET(userAvatar)
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/wait.gif')

        params = [
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('summary', valueOrDefault(edit, 'summary', extract(dialog, self.patterns['edit_summary']))),
            NVPair('issuetype', valueOrDefault(edit, 'issuetype', extract(dialog, self.patterns['edit_issuetype']))),
            NVPair('priority', valueOrDefault(edit, 'priority', extract(dialog, self.patterns['edit_priority']))),
            NVPair('assignee', valueOrDefault(edit, 'assignee', extract(dialog, self.patterns['edit_assignee']))),
            NVPair('environment', ''),
            NVPair('description', ''),
            NVPair('duedate', ''),
            NVPair('comment', ''),
            NVPair('commentLevel', '')
        ]
        if self.patterns['edit_reporter'].matcher(dialog).find():
            NVPair('reporter', valueOrDefault(edit, 'reporter', extract(dialog, self.patterns['edit_reporter']))),

            
        req.POST('/secure/QuickEditIssue.jspa?issueId=' + self.issueId + '&decorator=none', params, ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ))
        
        return self.view(cached)
