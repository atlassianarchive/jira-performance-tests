from env import request, extract
from java.util.regex import Pattern

class UserProfile:
    
    def __init__(self, testIndex):
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view user profile')
        }
        self.patterns = {
            'streams-gadget' : Pattern.compile('id="gadget-stream".*?src="http:\/\/.*?(\/.*?)"')
        }
        
    def view(self, username, cached=False):
        req = self.requests['view']
        
        page = req.GET('/secure/ViewProfile.jspa').text
        req.GET(extract(page, self.patterns['streams-gadget'], (('&amp;', '&'),)) )
        # uncomment when we fix activity streams
        # req.GET('/rest/activity-stream/1.0/url?_=1328593137337&numofentries=20&username=' + username)
        # req.GET('/plugins/servlet/streams?filterUser=' + username + '&os_authType=basic&maxResults=20&_=1328593137354')
        req.GET('/s/472/1/2/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date-en-US.js?_=1328593137335')
        
        if not cached:
            req.GET('/s/472/1/1/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/images/icons/bullet_blue.gif')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/status_assigned.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/472/1/4.0.2/_/images/gadgets/loading.gif')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/feedicon.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/showcomment.png')
