from env import request, extract, host
from HTTPClient import NVPair
from java.util.regex import Pattern
from java.lang import String

class Dashboard:
    
    def __init__(self, testIndex):
        self.requests = {
            'home_not_logged_in' : request(testIndex, 'HTTP-REQ : dashboard not logged in'),
            'login' : request(testIndex + 1, 'HTTP-REQ : perform login'),
            'home_logged_in' : request(testIndex + 2, 'HTTP-REQ : dashboard logged in'),
        }
        self.gadgetRegex = {
            'login' : Pattern.compile('id":"0.*?renderedGadgetUrl":"http:\/\/.*?(\/.*?)"'),
            'intro' : Pattern.compile('id":"10000.*?renderedGadgetUrl":"http:\/\/.*?(\/.*?)"'),
            'stream' : Pattern.compile('id":"10001.*?renderedGadgetUrl":"http:\/\/.*?(\/.*?)"'),
            'assignedToMe' : Pattern.compile('id":"10002.*?renderedGadgetUrl":"http:\/\/.*?(\/.*?)"'),
            'favFilters' : Pattern.compile('id":"10003.*?renderedGadgetUrl":"http:\/\/.*?(\/.*?)"'),
            'admin' : Pattern.compile('id":"10004.*?renderedGadgetUrl":"http:\/\/.*?(\/.*?)"')
        }
        
    # go to JIRA home and log in
    def login(self, username, password, cached=False):
        self.requests['login'].POST('/rest/gadget/1.0/login', 
                                    ( NVPair('os_username', username), NVPair('os_password', password), ),
                                    ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), )
                                )
        

    def loggedIn(self, cached=False):
        req = self.requests['home_logged_in']

        req.GET('/')
        page = req.GET('/secure/Dashboard.jspa').text
        
        req.GET(extract(page, self.gadgetRegex['intro'], (('&amp;', '&'),)) )
        req.GET(extract(page, self.gadgetRegex['stream'], (('&amp;', '&'),)) )
        req.GET(extract(page, self.gadgetRegex['assignedToMe'], (('&amp;', '&'),)) )
        req.GET(extract(page, self.gadgetRegex['favFilters'], (('&amp;', '&'),)) )
        req.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        # req.GET('/rest/activity-stream/1.0/url?_=1328062116381&keys=__all_projects__&numofentries=5&username=')
        req.GET('/rest/gadget/1.0/favfilters?_=1328062116355&showCounts=true')
        req.GET('/rest/gadget/1.0/intro?_=1328062116368')
        req.GET('/rest/gadget/1.0/issueTable/jql?_=1328062116419&jql=assignee+=+currentUser()+AND+resolution+=+unresolved+ORDER+BY+priority+DESC,+created+ASC&num=10&addDefault=true&enableSorting=true&sortBy=null&paging=true&startIndex=0&showActions=false')
        # ignore till we fix streams
        # req.GET('/plugins/servlet/streams?os_authType=basic&maxResults=5&_=1328062116479')
        req.GET('/s/472/1/2/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date-en-US.js?_=1328062116380')

        if self.gadgetRegex['admin'].matcher(page).find():
            req.GET('/rest/gadget/1.0/admin?_=1328062116344')
            req.GET(extract(page, self.gadgetRegex['admin'], (('&amp;', '&'),)) )
            
        if not cached:
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.1.2/_/download/resources/com.atlassian.gadgets.dashboard:dashboard/images/icons/tools_20.png')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/472/1/4.0.2/_/images/gadgets/loading.gif')
            req.GET('/images/intro-gadget.png')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/feedicon.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/icons/aui-icon-tools.gif')

    # go to home page, which redirects to dashbaord
    def notLoggedIn(self, cached=False):
        req = self.requests['home_not_logged_in']
        
        req.GET('/')
        page = req.GET('/secure/Dashboard.jspa').text

        req.GET( extract(page, self.gadgetRegex['intro'], (('&amp;', '&'),)) )
        req.GET( extract(page, self.gadgetRegex['login'], (('&amp;', '&'),)) )
        req.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        req.GET('/rest/gadget/1.0/intro?_=1328057537748')
        req.GET('/rest/gadget/1.0/login?_=1328057537760')

        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/472/1/4.0.2/_/images/gadgets/loading.gif')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            req.GET('/images/intro-gadget.png')

    def gadget(self, page, regex):
        url = extract(page, regex, (('&amp;', '&'),))
        parts = String(url).split("\\?")
        params = parts[1].split("&")
        gadget = parts[0] + '?'
        for param in params:
            kv = param.split('=')
            gadget += kv[0] + '=' + kv[1] + '&'
            
        return gadget
            
