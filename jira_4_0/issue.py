from HTTPClient import NVPair
from env import request, extract, valueOrEmpty, valueOrDefault, postMultipart, extractAll
from java.util.regex import Pattern
from java.lang import String
from form import extractOptions, extractSelects, extractSubmit, extractTextAreas, extractTextfields

class Issue:
    
    def __init__(self, testIndex, issueKey, issueId):
        self.issueKey = issueKey
        self.issueId = issueId
        self.projectKey = String(issueKey).split("-")[0]
        
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view issue'),
            'add_comment' : request(testIndex + 1, 'HTTP-REQ : add comment to issue'),
            'edit' : request(testIndex + 2, 'HTTP-REQ : edit issue'),
            'workflow' : request(testIndex + 3, 'HTTP-REQ : workflow')
        }
        self.patterns = {
            'stream' : Pattern.compile('id="gadget-stream.*?src="http:\/\/.*?(\/.*?)"'),
            'edit_summary' : Pattern.compile('(?s)id="summary".*?value="(.*)"'),
            'edit_issuetype' : Pattern.compile('(?s)id="issuetype.*?selected="selected".*?value="([0-9]*)"'),
            'edit_priority' : Pattern.compile('(?s)id="priority".*?selected.*?value="([0-9]*)"'),
            'edit_assignee' : Pattern.compile('(?s)id="assignee".*?selected.*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)id="description".*?\>(.*?)\<'),
            'edit_reporter' : Pattern.compile('(?s)id="reporter".*?value="(.*?)"'),
            'workflow_action' : Pattern.compile('\/secure\/WorkflowUIDispatcher\.jspa.*?action=([0-9]*)'),
            'workflow_form' : Pattern.compile('(?s)form (action="CommentAssignIssue.*?)<\/form'),            
            'is_closed' : Pattern.compile('(?s)\/images\/icons\/status_closed.gif')
        }


    def getKey(self):
        return self.issueKey
    

    def view(self, cached=False):
        req = self.requests['view']
        page = req.GET('/browse/'+self.issueKey).text
        
        if not cached:
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/icons/aui-icon-tools.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/pincomment.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')                
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/images/icons/undo_16.gif')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
        actions = extractAll(page, self.patterns['workflow_action'])
        return { 
            'workflow_actions' : actions,
            'editable' : not self.patterns['is_closed'].matcher(page).find()
        }


    def nextWorkflowStep(self, stepId, values={}, cached=False):
        req = self.requests['workflow']
        target = req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=' + stepId).getHeader('Location')
        
        # workflow action without screen mutates on GET
        if not String(target).contains("browse"):
            # extract all form data and construct POST request
            page = req.GET(target).text
            form = extract(page, self.patterns['workflow_form'])
            params = []
            
            for select in extractSelects(form):
                name = select[0]
                options = extractOptions(select[1])
                params.append(NVPair(name, valueOrDefault(values, name, options[0])))
            
            for textfield in extractTextfields(form):
                params.append(NVPair(textfield, valueOrEmpty(values, textfield)))
                
            for textarea in extractTextAreas(form):
                params.append(NVPair(textarea, valueOrEmpty(values, textarea)))
                
            params.append(NVPair(extractSubmit(form), ''))
            params.append(NVPair('action', stepId))
            params.append(NVPair('id', self.issueId))
            
            postMultipart(req, '/secure/CommentAssignIssue.jspa', params)
        
        self.view(cached)
        
    
    def viewTabAll(self, cached=False):
        req = self.requests['view']
        req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.css')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.js')
            req.GET('/s/472/1/_/includes/js/pincomment.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
    def viewTabComments(self, cached=False):
        req = self.requests['view']
        req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/pincomment.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
    def viewTabChangeHistory(self, cached=False):
        req = self.requests['view']
        req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:changehistory-tabpanel')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/pincomment.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
    def viewTabActivityStream(self, cached=False):
        req = self.requests['view']
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.streams.streams-jira-plugin:activity-stream-issue-tab').text
        
        req.GET(extract(page, self.patterns['stream'], ( ('&amp;', '&'), )))
        req.GET('/plugins/servlet/streams?key=' + self.projectKey + '&itemKey=' + self.issueKey + '&os_authType=basic&maxResults=20&_=1328149721131')
        req.GET('/rest/activity-stream/1.0/url?_=1328149721084&keys=' + self.projectKey + '&itemKeys=' + self.issueKey + '&numofentries=20&username=')
        req.GET('/s/472/1/2/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date-en-US.js?_=1328149721083')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/pincomment.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/4.0.2/_/images/gadgets/loading.gif')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/showcomment.png')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/feedicon.png')
            
    def addComment(self, comment={ 'comment' : 'test' }, cached=False):
        self.requests['add_comment'].POST('/secure/AddComment.jspa',
            (
                NVPair('comment', valueOrEmpty(comment, 'comment')),
                NVPair('commentLevel', valueOrEmpty(comment, 'commentLevel')),
                NVPair('id', self.issueId),
                NVPair('Add ', 'Add '),
            ),
            ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) 
        )
        
        self.view(cached)


    def edit(self, edit={}, cached=False):
        req = self.requests['edit']
        page = req.GET('/secure/EditIssue!default.jspa?id=' + self.issueId).text
        
        if not cached:
            req.GET('/s/472/1/1/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/472/1/1.0/_/download/resources/jira.webresources:calendar/calendar.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/help_blue.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/cal.gif')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')

        params = [
            NVPair('id', self.issueId),
            NVPair('summary', valueOrDefault(edit, 'summary', extract(page, self.patterns['edit_summary']))),
            NVPair('issuetype', valueOrDefault(edit, 'issueType', extract(page, self.patterns['edit_issuetype']))),
            NVPair('priority', valueOrDefault(edit, 'priority', extract(page, self.patterns['edit_priority']))),
            NVPair('assignee', valueOrDefault(edit, 'assignee', extract(page, self.patterns['edit_assignee']))),
            NVPair('description', valueOrDefault(edit, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('attachment_field.1', ''),
            NVPair('attachment_field.2', ''),
            NVPair('attachment_field.3', ''),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
            NVPair('duedate', ''),
            NVPair('environment', ''),
            NVPair('Update', 'Update')
        ]
        if self.patterns['edit_reporter'].matcher(page).find():
            params.append(NVPair('reporter', valueOrDefault(edit, 'reporter', extract(page, self.patterns['edit_reporter']))))
            
        postMultipart(req, '/secure/EditIssue.jspa', params)

        self.view(cached)
