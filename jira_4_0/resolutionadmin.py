from HTTPClient import NVPair
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class ResolutionAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view resolutions'),
            'add' : request(testId + 1, 'HTTP-REQ : add resolution'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit resolution')
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
        }

    def browse(self, cached=False):
        req = self.requests['browse']
        
        req.GET('/secure/admin/ViewResolutions.jspa')
        
        if not cached:
            req.GET('/s/472/1/9/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/_/includes/js/adminMenu.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/9/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/images/icons/arrow_up_blue_small.gif')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/472/1/_/images/icons/help_blue.gif')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/_/images/icons/navigate_right_10.gif')
            req.GET('/s/472/1/_/images/icons/bullet_creme.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')


    def add(self, resolution, cached=False):
        self.requests['add'].POST('/secure/admin/AddResolution.jspa',
            (
                NVPair('name', resolution['name']),
                NVPair('description', valueOrEmpty(resolution, 'description')),
                NVPair('Add', 'Add'),
            ), (
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))

        self.browse(cached)


    def edit(self, resolution, cached=False):
        req = self.requests['edit']
        rid = resolution['id']
        
        page = req.GET('/secure/admin/EditResolution!default.jspa?id=' + rid).text
        
        if not cached:
            req.GET('/s/472/1/9/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/adminMenu.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/472/1/_/images/icons/navigate_right_10.gif')
            req.GET('/s/472/1/9/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
                
        req.POST('/secure/admin/EditResolution.jspa',
            (
                NVPair('name', valueOrDefault(resolution, 'name', extract(page, self.patterns['edit_name']))),
                NVPair('description', valueOrDefault(resolution, 'description', extract(page, self.patterns['edit_description']))),
                NVPair('Update', 'Update'),
                NVPair('id', rid),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
        
        self.browse(cached)
        