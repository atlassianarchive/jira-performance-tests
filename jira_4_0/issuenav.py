from HTTPClient import NVPair
from env import request, valueOrEmpty

class IssueNavigator:
    
    def __init__(self, testIndex):
        self.requests = {
            'simple_search' : request(testIndex, 'HTTP-REQ : issue navigator simple search'),
            'advanced_search' : request(testIndex + 1, 'HTTP-REQ : issue navigator advanced search')
        }
        
    def simpleSearch(self, query={}, cached=False):
        req = self.requests['simple_search']
        
        req.GET('/secure/IssueNavigator.jspa?mode=show&createNew=true')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/472/1/1.0/_/download/resources/jira.webresources:calendar/calendar.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/includes/js/cascadingUtil.js')
            req.GET('/includes/js/issuetypes-search.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/help_blue.gif')
            req.GET('/s/472/1/_/images/icons/information.gif')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/status_inprogress.gif')
            req.GET('/images/icons/status_resolved.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/status_closed.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/cal.gif')
            req.GET('/images/icons/duedatepicker-icon.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/_/images/icons/view_20.png')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/tools_20.png')
            req.GET('/s/472/1/_/images/icons/permalink_light_16.png')
            req.GET('/images/icons/down.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/s/472/1/_/images/icons/tools_12.png')
        
        req.POST('/secure/IssueNavigator.jspa?', (
                    NVPair('show', 'View >>'),
                    NVPair('pid', valueOrEmpty(query, 'pid')),
                    NVPair('type', valueOrEmpty(query, 'type')),
                    NVPair('query', valueOrEmpty(query, 'query')),
                    NVPair('summary', valueOrEmpty(query, 'summary')),
                    NVPair('description', valueOrEmpty(query, 'description')),
                    NVPair('body', valueOrEmpty(query, 'body')),
                    NVPair('environment', valueOrEmpty(query, 'environment')),
                    NVPair('fixfor', valueOrEmpty(query, 'fixfor')),
                    NVPair('component', valueOrEmpty(query, 'component')),
                    NVPair('version', valueOrEmpty(query, 'version')),
                    NVPair('reporterSelect', valueOrEmpty(query, 'reporterSelect')),
                    NVPair('assigneeSelect', valueOrEmpty(query, 'assigneeSelect')),
                    NVPair('status', valueOrEmpty(query, 'status')),
                    NVPair('resolution', valueOrEmpty(query, 'resolution')),
                    NVPair('priority', valueOrEmpty(query, 'priority')),
                    NVPair('created:after', valueOrEmpty(query, 'created:after')),
                    NVPair('created:before', valueOrEmpty(query, 'created:before')),
                    NVPair('created:previous', valueOrEmpty(query, 'created:previous')),
                    NVPair('created:next', valueOrEmpty(query, 'created:next')),
                    NVPair('updated:after', valueOrEmpty(query, 'updated:after')),
                    NVPair('updated:before', valueOrEmpty(query, 'updated:before')),
                    NVPair('updated:previous', valueOrEmpty(query, 'updated:previous')),
                    NVPair('updated:next', valueOrEmpty(query, 'updated:next')),
                    NVPair('duedate:after', valueOrEmpty(query, 'duedate:after')),
                    NVPair('duedate:before', valueOrEmpty(query, 'duedate:before')),
                    NVPair('duedate:previous', valueOrEmpty(query, 'duedate:previous')),
                    NVPair('duedate:next', valueOrEmpty(query, 'duedate:next')),
                    NVPair('resolutiondate:after', valueOrEmpty(query, 'resolutiondate:after')),
                    NVPair('resolutiondate:before', valueOrEmpty(query, 'resolutiondate:before')),
                    NVPair('resolutiondate:previous', valueOrEmpty(query, 'resolutiondate:previous')),
                    NVPair('resolutiondate:next', valueOrEmpty(query, 'resolutiondate:next')),
                    NVPair('workratio:min', valueOrEmpty(query, 'workratio:min')),
                    NVPair('workratio:max', valueOrEmpty(query, 'workratio:max')),
                ),
                ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) 
        )

        
    def advancedSearch(self, jql, cached=False):
        req = self.requests['advanced_search']

        req.GET('/secure/IssueNavigator.jspa')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jqlautocomplete/jira.webresources:jqlautocomplete.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/bullet_red.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/s/472/1/_/images/icons/view_20.png')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/permalink_light_16.png')
            req.GET('/s/472/1/_/images/icons/tools_20.png')
            req.GET('/s/472/1/_/images/icons/help_blue.gif')
            req.GET('/s/472/1/_/images/icons/accept.png')
            req.GET('/images/icons/down.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/472/1/_/images/icons/tools_12.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
                
        req.POST('/secure/IssueNavigator!executeAdvanced.jspa', 
             ( NVPair('jqlQuery', jql), ), 
             ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
