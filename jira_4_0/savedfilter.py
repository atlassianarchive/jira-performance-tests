from env import request

class SavedFilter:
    
    def __init__(self, testIndex):
        self.requests = {
            'manage' : request(testIndex, 'HTTP-REQ : manage saved filters'),
            'use' : request(testIndex + 1, 'HTTP-REQ : use a saved filter')
        }
        
    def manage(self, cached=False):
        req = self.requests['manage']
        
        req.GET('/secure/ManageFilters.jspa')
        
        if not cached:
            req.GET('/s/472/1/1/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/1/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/help_blue.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/s/472/1/_/images/icons/information.gif')
            req.GET('/s/472/1/_/images/icons/star_yellow.gif')
            req.GET('/images/icons/filter_private.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
            
    def use(self, savedFilterId, cached=False):
        req = self.requests['use']
        
        req.GET('/secure/IssueNavigator.jspa?mode=hide&requestId=' + str(savedFilterId))
        
        if not cached:
            req.GET('/s/472/1/2/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/s/472/1/2/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/_/images/icons/star_yellow.gif')
            req.GET('/s/472/1/_/images/icons/bullet_blue.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/s/472/1/_/images/icons/tools_20.png')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/view_20.png')
            req.GET('/images/icons/down.gif')
            req.GET('/s/472/1/_/images/icons/permalink_light_16.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/472/1/_/images/icons/tools_12.png')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
        