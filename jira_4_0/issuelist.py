from issue import Issue
from java.lang import Math  

class IssueList:
    
    def __init__(self, baseTestIndex, config):
        self.issues = []
        self.config = config
        self.MAX_ISSUES = 500
        
        rowCount = self.getRowCount(config.getIssueFile())
        bounds = self.getBounds(rowCount, config.getTotalWorkers(), config.getCurrentWorker())
        print 'current worker #' + str(config.getCurrentWorker()) + ' (out of ' + str(config.getTotalWorkers()) + ') uses issues between #' + str(bounds[0]) + ' and #' + str(bounds[1]) 
        
        f = open(config.getIssueFile(), 'r')
        try:
            for i in range(rowCount):
                line = f.readline()
                if i > bounds[1]:
                    break
                
                if i >= bounds[0]:
                    issueData = line.split(',')
                    print 'adding issue ' + issueData[0]
                    self.issues.append(Issue(baseTestIndex, issueData[0], issueData[1].replace('\n', '')))
            
        finally:
            f.close()
            
    def getIssues(self):
        return self.issues
    
    def getBounds(self, rowCount, workerCount, workerIndex):
        issuesPerWorker = rowCount / workerCount
        i = issuesPerWorker * workerIndex
        return (i, Math.min(i + issuesPerWorker, i + self.MAX_ISSUES))
        
    def getRowCount(self, path):
        f = open(path, 'r')
        try:
            for i, l in enumerate(f):
                pass
            
            return i+1
        finally:
            f.close()
